"use strict";
let express = require("express");
let router = express.Router();
let path = require("path");
let database = require(path.join("..", "./database"));
let session = require("express-session");
//Creates a session
router.use(
  session({
    resave: true,
    saveUninitialized: true,
    secret: "SECRETSESSION",
    cookie: { maxAge: 60000 }
  })
);
router.post("/loginaction", function(req, res) {
    let loginDetails = req.body;
    database.login(loginDetails, function(err, validDetails) {
      //Condition for valid Login data
      if (validDetails) {
        //validDetails consist of ---> { firstname,lastname,username,password }
  
        //assigns a session for a user
        sess = req.session;
  
        //stores validDetails of user in session
        sess.userDetails = validDetails;
        //console.log(sess.userDetails.firstname);
  

      } else {
        //Asks to Login on failing condition above
        res.send(
          '<div align="center">Invalid Login , Please Re-Enter Details or Register a New Account</div>'
        );
      }
    });
  });
router.post("/register", function(req, res) {
    let regDetails = req.body;
    console.log(req.body)
    database.register(regDetails, function(decide) {
      //checks the value if username already exists
      if (decide == "success") {
        //Registration Successful!!
        res.json({ success: true, message: 'User créer' });
      } else {
        //if user already exists in database , asks to login
        res.json({ success: false, message: 'User already exist' });
      }
    });
  });
  module.exports = router;