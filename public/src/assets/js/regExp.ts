export function checkMail(controls) {
    const regExp = new RegExp(/\S+@\S+\.\S+/);
    if (regExp.test(controls.value)) {
      return null;
    } else {
      return { checkMail: true };
    }
  }
export function   checkpw(controls) {
    const regExp = new RegExp(/^[a-z0-9A-Z]+$/);
    if (regExp.test(controls.value)) {
      return null;
    } else {
      return { checkPseudo: true };
    }
  }
