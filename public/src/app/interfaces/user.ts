export default interface User {
    code_pays: string;
    date_naissance: Date;
    device_type: string;
    email: string;
    nom: string;
    password: string;
    plateforme: string;
    prenom: string;
    recevoir_email: boolean;
    sexe: string;
}
