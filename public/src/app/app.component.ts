import { Component, OnInit, HostListener } from '@angular/core';
import * as AOS from 'aos';
import { ToastrService } from 'ngx-toastr';
import { Router, NavigationEnd } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'client';
  fixe = '';
  constructor(
    private toast: ToastrService,
    private router: Router) {
    this.statusOffline();
    this.statusOnline();
    window.scroll(0, 0);
  }
  @HostListener('window:scroll', [])
  onWindowScroll() {
    const scroll = document.documentElement.scrollTop;
    if (scroll > 100) {
      this.fixe = 'fixe fade-in';
    } else if (scroll < 5) {
      this.fixe = 'fade-out';
    }
  }
  statusOnline() {
    window.addEventListener('online', (resp: any) => {
      this.toast.success('Connected');
    });
  }

  statusOffline() {
    window.addEventListener('offline', () => {
      this.toast.error('Offline');
    });
  }
  ngOnInit() {
    AOS.init();
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return window.scroll(0, 0);
      }
    });
  }

}
