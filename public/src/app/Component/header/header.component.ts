import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { BreakpointObserver } from '@angular/cdk/layout';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  menuList: string[];
  name: string;
  signe = 'password';
  modalMobilMenu: BsModalRef;
  faLogin = 'fa-eye';
  accountClass = '';
  newClass = 'activModal';
  MselectSexe = 'selectedSexe';
  FselectSexe = 'unselectedSexe';
  menuWS = ['Accueil' , 'Blog', 'évenement', 'FAQ', 'CGU'];
  modalRef: BsModalRef;
  modalRef2: BsModalRef;
  formRegister: FormGroup;
  votreEmail: any;
  mdp: any;
  sexe = 'HOMME';
  currentUserSubject: BehaviorSubject<any>;
  currentUser: any;
  newsCustomer: boolean;
  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private router: Router,
    public breakpointObserver: BreakpointObserver,
    private modalService: BsModalService) {
    this.createformRegister();
    this.currentUserSubject = new BehaviorSubject<any>(localStorage.getItem('token'));
    if (this.currentUserSubject.getValue() !== null) {
      this.menuList = ['Votre compte', 'Votre liste d’envies', 'Vos messages', 'Se déconnecter'];
    } else {
      this.menuList = ['Je suis nouveau client', 'J\'ai déjà un compte'];
      this.name = ', Identifiez-vous';
    }
    this.currentUser = this.currentUserSubject.asObservable();
  }
  createformRegister() {
    this.formRegister = this.formBuilder.group({
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      email: ['', Validators.compose([
        Validators.required,
        this.checkMail
      ])],
      password: ['', Validators.compose([
        Validators.required,
        this.checkpw
      ])],
    });
  }
  checkMail(controls) {
    const regExp = new RegExp(/\S+@\S+\.\S+/);
    if (regExp.test(controls.value)) {
      return null;
    } else {
      return { checkMail: true };
    }
  }
  checkpw(controls) {
    const regExp = new RegExp(/^[a+A-Z+0-9]+$/);
    if (regExp.test(controls.value)) {
      return null;
    } else {
      return { checkPseudo: true };
    }
  }
  openModal(template: TemplateRef<any>) {
    console.log(template)
    this.modalRef = this.modalService.show(template,
      Object.assign({}, { class: 'burgermenu' }));
  }
  selectSexe() {
    if (this.MselectSexe === 'selectedSexe' && this.FselectSexe === 'unselectedSexe') {
      this.FselectSexe = 'selectedSexe';
      this.MselectSexe = 'unselectedSexe';
      this.sexe = 'FEMME';
    } else if (this.FselectSexe === 'selectedSexe' && this.MselectSexe === 'unselectedSexe') {
      this.FselectSexe = 'unselectedSexe';
      this.MselectSexe = 'selectedSexe';
      this.sexe = 'HOMME';
    }
  }
  loginModal(template: TemplateRef<any>) {
    this.modalRef2 = this.modalService.show(
      template,
      Object.assign({ backdrop: 'static' }, { class: 'login' }),
    );
    if (this.modalRef !== undefined) {
      this.modalRef.hide();
    }
  }
  showPassLogin() {
    if (this.signe === 'password' && this.faLogin === 'fa-eye') {
      this.signe = 'text';
      this.faLogin = 'fa-eye-slash';
    } else {
      this.signe = 'password';
      this.faLogin = 'fa-eye';
    }
  }
  goTo(a, loginTemplate) {
    if (a.includes('Votre compte')) {
      console.log('click votre compte')
    } else if (a.includes('nouveau client')) {
      this.loginModal(loginTemplate);
      this.newsCustomer = true;
      this.newsCustomerBtn();
    } else if (a.includes('déjà un compte')) {
      this.loginModal(loginTemplate);
      this.newsCustomer = false;
      this.newsCustomerBtn();
    } else if (a.includes('déconnecter')) {
      this.logout();
    } else if (a.includes('reset-mdp')) {
      this.modalRef2.hide();
      this.router.navigate(['/reset-mdp']);
    }
  }
  newsCustomerBtn() {
    if (this.newsCustomer === false) {
      this.newsCustomer = true;
      this.newClass = '';
      this.accountClass = 'activModal';
    } else if (this.newsCustomer === true) {
      this.newsCustomer = false;
      this.newClass = 'activModal';
      this.accountClass = '';
    }
  }
  logout() {
    localStorage.removeItem('token');
    this.currentUserSubject.next(null);
    this.menuList = ['Je suis nouveau client', 'J\'ai déjà un compte'];
    this.name = ', Identifiez-vous';
    this.toastr.warning('à Bientot', '', {
      timeOut: 1000
    });

  }
  goBackM(a) {
    this.openModal(a);
    this.modalMobilMenu.hide();
  }
  ngOnInit(): void {
  }

}
