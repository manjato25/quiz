import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private dataS: DataService
  ) { }

  ngOnInit(): void {
    this.dataS.registerUser().subscribe(data55555 => {
      console.log(data55555, 'data55555data55555data55555');
    });
    this.dataS.getJSON().subscribe(data => {
      console.log(data);
    });

  }

}
