import { Component, OnInit } from '@angular/core';
import { BsDropdownConfig } from 'ngx-bootstrap/dropdown';

@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.scss'],
  providers: [{ provide: BsDropdownConfig, useValue: { isAnimated: true, autoClose: true } }]
})
export class AboutusComponent implements OnInit {
  services = ['Newsletter', 'CB acceptation', 'Entretien', 'Nos catégories'];
  espaces = ['Mons compte', 'Espace Vendeurs', 'Contactez - nous', 'Livraison'];
  connaitres = ['Qui Sommes Nous', 'CGU - CGV', 'Plan du site', 'Mentions légales'];
  panelOpenState = false;
  constructor() { }

  ngOnInit(): void {
  }

}
